<?php

namespace app\lib;


abstract class Helpers
{
    protected static function get()
    {
        if (count($_GET) > 0)
            foreach ($_GET as $k => $v) {
                return $k;
            }
        return false;
    }
    protected static function json($val, bool $echo = true)
    {
        $json = json_encode($val, JSON_UNESCAPED_UNICODE);
        if (!$echo)
            return $json;
        echo $json;
        exit;
    }
}
