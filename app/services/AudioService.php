<?php

namespace app\services;


class AudioService
{
    public static function getDir()
    {
        $dir = array_diff(scandir(MAIN_DIR . 'public_html/media'), ['..', '.', '.gitignore']);
        $list = [];

        foreach ($dir as $i) {

            $path = MAIN_DIR . "public_html/media/{$i}";
            $data = json_decode(file_get_contents("{$path}/info.json"), true);
            $songs = array_diff(scandir($path), ['..', '.', 'folder.jpg', 'info.json']);
            $song_list = [];

            foreach ($songs as $song) {
                $s = [
                    'name' => $song,
                    'path' => "./media/{$i}/{$song}"
                ];
                array_push($song_list, $s);
            }

            $subdir = [
                'name' => $data['album'],
                'artist' => $data['artist'],
                'path' => "./media/{$i}/",
                'songs' => $song_list
            ];
            array_push($list, $subdir);
        }

        return $list;
    }
}
