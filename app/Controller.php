<?php

namespace app;

use app\lib\Helpers as H;
use app\services\AudioService;


class Controller extends H
{
    public static function init()
    {
        switch (H::get())
        {
            case 'get-dir':
                H::json(AudioService::getDir());

            default:
                require_once MAIN_DIR . 'app/views/index.html';
        }
    }
}
