<?php

define('MAIN_DIR', dirname(dirname(__FILE__)) . '/');

require_once MAIN_DIR . 'config.inc.php';

app\Controller::init();
