# Rockola

## Librerías
-   PHP 7.4 - Servicios
-   Vue 2 - Backend
-   Vuex - Backend
-   Bootstrap 4 - Frontend
-   Node 14 - Compilador

## Instalación

Descargar el repositorio en el servidor

```bash
git clone https://tr3nt@bitbucket.org/tr3nt/rockola.git
```

## Biblioteca Multimedia

Agregar los discos en folders con 2 archivos necesarios:
-   __info.json__ <- Con nombre del artista y nombre del album
-   __folder.jpg__ <- Imagen de la portada del disco

![Archivos](src/media/img01.png)


Formato del archivo __info.json__

```json
{
	"album": "Tan Chidos",
	"artist": "Liliana Felipe"
}
```

## Funcionamiento

Los albumes serán agregados automáticamente

![Vista 1](src/media/img02.png)

Al hacer click sobre la portada se mostrarán las canciones

![Vista 2](src/media/img03.png)

Al hacer click sobre las canciones se agregarán a la lista de reproducción

![Vista 3](src/media/img04.png)

## Authors

Ezhaym N - Musician & Laravel developer
