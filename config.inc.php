<?php

session_start();

spl_autoload_register(function ($className) {
    include MAIN_DIR . str_replace('\\', '/', $className) . ".php";
});
