import Vue from 'vue'

import { Plugin } from 'vue-fragment'
Vue.use(Plugin);

import store from './store'

import $ from 'jquery'
window.$ = window.jQuery = $;

import Popper from 'popper.js'
window.Popper = Popper;

import 'bootstrap'

import 'bootstrap/dist/css/bootstrap.css'
import './app.css'

import App from './App'

const app = new Vue({
    render: h => h(App),
    store
}).$mount('#app');
