import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        audio: { src: '' },
        songList: [],
        ended: true,
        marquee: ''
    },
    getters: {
        SONG_LIST: state => {
            return state.songList
        },
        MARQUEE: state => {
            return state.marquee
        },
        ENDED: state => {
            return state.ended
        }
    },
    mutations: {
        REMOVE_SONG_LIST: (state, song) => {
            state.songList.splice(song, 1);
        },
        ADD_SONG_LIST: (state, song) => {
            state.songList.push(song);
        },
        CLEAN_SONG_LIST: state => {
            state.songList = []
        }
    },
    actions: {
        PLAY: ({ state, commit, dispatch }) => {
            state.audio.src = '';
            if (state.songList.length) {
                let song = state.songList.shift();
                state.audio = new Audio(song.path);
                state.ended = false;
                state.audio.addEventListener("ended", () => {
                    state.ended = true;
                    state.marquee = '';
                    dispatch('PLAY');
                });
                state.marquee = song.name.slice(0, -4);
                state.audio.play();
            }
        },
        STOP: ({ state }) => {
            state.audio.src = '';
            state.marquee = '';
            state.ended = true;
        }
    }
});

export default store;
