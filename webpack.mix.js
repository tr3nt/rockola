let mix = require('laravel-mix');

mix
    .setPublicPath('public_html/dist')
    .js('src/main.js', 'app.js')
    .vue();
